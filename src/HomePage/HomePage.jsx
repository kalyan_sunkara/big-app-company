import React from 'react';
import { Link } from 'react-router-dom';
import {Col,Row} from 'antd'

import { userService } from '../_services';
import { LoginPage } from '../LoginPage';


class HomePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {},
            users: []
        };
    }

    componentDidMount() {
        this.setState({ 
            user: JSON.parse(localStorage.getItem('user')),
            users: { loading: true }
        });
        fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json())
        
        .then(data => this.setState({ users:data }));
       
       
        
    }

    render() {
        const { user, users } = this.state;
        return (
            <div className="col-md-6 col-md-offset-3">
            
                {users.loading && <em>Loading users...</em>}
                {users.length &&
                   
                <div class="container">
                <h2>DashBoard Page</h2>

                <table class="table">
                    <thead>
                        <tr >
                            <th>Id</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>UserName</th>
                            <th>CompanyName</th>
                            
            
                        </tr>
                    </thead>
                    <tbody >
                        {console.log("users",users)}
                        
                        {users.map((item,index)=>{
                        return( <tr key={index}>
                        <td >{item.id}</td>
                            <td >{item.name}</td> <td >{item.phone}</td> <td >{item.username}</td> 
                        <td>{item.company.name}</td>
                            </tr>
                        );
                    })}

                    </tbody>
                </table>
            </div>
    
                }
                <p>
                    <Link to="/login">Logout</Link>
                </p>
                <p>
                    <Link to="/drag">DragAndDrop</Link>
                </p>
            </div>
        );
    }
}

export { HomePage };